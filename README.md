This is the working directory of the project **Less Typing, More Thinking!** part of the CERN's Web Fest 2020. 

Happy Hacking!

Some hints (because the first steps are always the harder): 
1. in the terminal navigate to this project main folder
   `cd ~/workspace/LTMT`
2. to create:
   `python3 -m venv venv`
3. to activate:
   `source venv/bin/activate`
4. to install packages in it:
   `pip install flask`
